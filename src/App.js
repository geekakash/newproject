import React,{useState, useRef,useEffect} from 'react';
import TodoList from'./TodoList';
import uuidv4 from 'uuid/v4';

const LOCAL_STORAGE_KEY = 'taskApp.tasks'

function App() {
  const [tasks,setTasks] = useState([])
  const taskNameRef = useRef()


  useEffect(() => {
    const storedTasks = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
    if (storedTasks) setTasks(storedTasks)
  }, [])

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(tasks))
  }, [tasks])

  function AddTask(){
    const name = taskNameRef.current.value 
    if(name === '') return
    setTasks(prevtasks =>{
      return [...prevtasks, {id: uuidv4(), name:name, done: false}]
    })
    taskNameRef.current.value= null

  }

  function toggletask(id){
    const newTasks = [...tasks]
    const task= newTasks.find(task => task.id === id)
    task.done=!task.done
    setTasks(newTasks)
  }

  function clearTasks(){
    const newTasks= tasks.filter(task => !task.done)
    setTasks(newTasks)
  }

  return (
    <div>
      <TodoList tasks={tasks} toggletask={toggletask}/>
      <input ref={taskNameRef} type="text"/>
      <button onClick={AddTask}>Add Task</button>
      <button onClick={clearTasks}>Clear</button>
      <div>{tasks.filter(task => !task.done).length} left to do</div>
    </div>
  );
}

export default App;
