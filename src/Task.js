import React from 'react'

export default function Task({task, toggletask}) {
    function  handleClick () {
        toggletask(task.id)
    }
    return (
        <div>
            <label>
             <input type = "checkbox"   checked = {task.done} onChange={handleClick}/>
            {task.name}
            </label>
            
        </div>
    )
}
