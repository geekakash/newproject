import React from 'react'
import Task from './Task'
export default function TodoList({tasks, toggletask}) {
    return (
      tasks.map( task =>{
          return <Task key={task.id} task={task} toggletask={toggletask}/>
      }

      )
    )
}
